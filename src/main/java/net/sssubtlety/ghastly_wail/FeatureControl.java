package net.sssubtlety.ghastly_wail;

import net.fabricmc.fabric.api.gamerule.v1.CustomGameRuleCategory;
import net.fabricmc.fabric.api.gamerule.v1.GameRuleFactory;
import net.fabricmc.fabric.api.gamerule.v1.GameRuleRegistry;

import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.util.random.RandomGenerator;
import net.minecraft.world.GameRules;

import static net.sssubtlety.ghastly_wail.GhastlyWail.NAMESPACE;

public final class FeatureControl {
    private FeatureControl() { }

    private static final CustomGameRuleCategory GHASTLY_WAIL_CATEGORY = new CustomGameRuleCategory(
        Identifier.of(NAMESPACE, "gamerule_category"),
        Text.translatable("gamerule.category." + NAMESPACE)
    );

    private static final GameRules.Key<GameRules.IntGameRule> OBSIDIAN_CONVERSION_CHANCE = GameRuleRegistry.register(
        NAMESPACE + ":obsidianConversionChance", GHASTLY_WAIL_CATEGORY,
        GameRuleFactory.createIntRule(100, 0, 100)
    );

    private static final GameRules.Key<GameRules.IntGameRule> BREAK_CONVERTED_OBSIDIAN = GameRuleRegistry.register(
        NAMESPACE + ":convertedObsidianBreakChance", GHASTLY_WAIL_CATEGORY,
        GameRuleFactory.createIntRule(100, 0, 100)
    );

    public static void init() { }

    public static boolean shouldConvertObsidian(ServerWorld world) {
        return rollPercentChance(world.getGameRules().get(OBSIDIAN_CONVERSION_CHANCE).getValue(), world.random);
    }

    public static boolean shouldBreakConvertedObsidian(ServerWorld world) {
        return rollPercentChance(world.getGameRules().get(BREAK_CONVERTED_OBSIDIAN).getValue(), world.random);
    }

    private static boolean rollPercentChance(int chance, RandomGenerator random) {
        if (chance <= 0) {
            return false;
        } else if (chance >= 100) {
            return true;
        } else {
            return chance >= random.range(0, 101);
        }
    }
}
