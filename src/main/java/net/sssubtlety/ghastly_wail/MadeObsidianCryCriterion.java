package net.sssubtlety.ghastly_wail;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.advancement.criterion.AbstractCriterionTrigger;
import net.minecraft.advancement.criterion.BeeNestDestroyedCriterionTrigger;
import net.minecraft.predicate.ContextAwarePredicate;
import net.minecraft.predicate.entity.EntityPredicate;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.dynamic.Codecs;

import java.util.Optional;

public class MadeObsidianCryCriterion extends AbstractCriterionTrigger<MadeObsidianCryCriterion.Conditions> {
    @Override
    public Codec<Conditions> getCodec() {
        return Conditions.CODEC;
    }

    public void trigger(ServerPlayerEntity player) {
        this.trigger(player, conditions -> true);
    }

    public record Conditions(Optional<ContextAwarePredicate> player) implements AbstractCriterionTrigger.Conditions {
        public static final Codec<Conditions> CODEC = RecordCodecBuilder.create(
            instance -> instance.group(
                EntityPredicate.ADVANCEMENT_CODEC.optionalFieldOf("player").forGetter(Conditions::player)
            ).apply(instance, Conditions::new)
        );
    }
}
