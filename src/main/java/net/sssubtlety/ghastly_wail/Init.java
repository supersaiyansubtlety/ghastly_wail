package net.sssubtlety.ghastly_wail;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.resource.ResourcePackActivationType;
import net.fabricmc.loader.api.FabricLoader;

import net.minecraft.util.Identifier;

import static net.fabricmc.fabric.api.resource.ResourceManagerHelper.registerBuiltinResourcePack;
import static net.sssubtlety.ghastly_wail.GhastlyWail.*;

public class Init implements ModInitializer {
    @Override
    public void onInitialize() {
        FeatureControl.init();

        GhastlyWail.init();

        FabricLoader.getInstance().getModContainer(NAMESPACE).ifPresent(ghastlyWailContainer ->
            registerBuiltinResourcePack(
                Identifier.of(NAMESPACE, "piglin_bartering_sans_crying_obsidian"),
                ghastlyWailContainer,
                ResourcePackActivationType.NORMAL
            )
        );
    }
}
