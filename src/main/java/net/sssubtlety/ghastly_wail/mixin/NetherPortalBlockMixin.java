package net.sssubtlety.ghastly_wail.mixin;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.NetherPortalBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.sssubtlety.ghastly_wail.mixin_helper.GhastEntityMixinAccessor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(NetherPortalBlock.class)
abstract class NetherPortalBlockMixin extends Block {
    private NetherPortalBlockMixin() {
        //noinspection DataFlowIssue
        super(null);
        throw new IllegalStateException("NetherPortalBlockMixin's dummy constructor called!");
    }

    @Inject(method = "onEntityCollision", at = @At("HEAD"))
    private void preOnEntityCollision(BlockState state, World world, BlockPos pos, Entity entity, CallbackInfo ci) {
        if (entity.getType() == EntityType.GHAST) ((GhastEntityMixinAccessor)entity).ghastly_wail$setLastPortalPos(pos);
    }
}
