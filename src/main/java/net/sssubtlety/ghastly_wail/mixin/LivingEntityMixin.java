package net.sssubtlety.ghastly_wail.mixin;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(LivingEntity.class)
abstract class LivingEntityMixin extends Entity {
    protected LivingEntityMixin() {
        // noinspection ConstantConditions
        super(null, null);
        throw new IllegalStateException("LivingEntityMixin's dummy constructor called!");
    }

    // handler stub to be overriden by GhastEntityMixin
    @Inject(method = "onDeath", at = @At("HEAD"))
    protected void preOnDeath(DamageSource source, CallbackInfo ci) { }
}
