package net.sssubtlety.ghastly_wail.mixin;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.NetherPortalBlock;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.GhastEntity;
import net.minecraft.entity.mob.Monster;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Pair;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;
import net.sssubtlety.ghastly_wail.GhastlyWail;
import net.sssubtlety.ghastly_wail.mixin_helper.GhastEntityMixinAccessor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.LinkedList;
import java.util.List;

import static net.sssubtlety.ghastly_wail.FeatureControl.shouldBreakConvertedObsidian;
import static net.sssubtlety.ghastly_wail.FeatureControl.shouldConvertObsidian;

@Mixin(GhastEntity.class)
abstract class GhastEntityMixin extends LivingEntityMixin implements Monster, GhastEntityMixinAccessor {
    @Unique private BlockPos lastPortalPos;

    // overrides stub handler in LivingEntityMixin
    @Override
    protected void preOnDeath(DamageSource source, CallbackInfo ci) {
        if (
            this.getWorld() instanceof ServerWorld world &&
            !this.noClip &&
            this.lastPortalPos != null &&
            world.getGameRules().getBooleanValue(GameRules.DO_MOB_GRIEFING)
        ) {
            if (this.getBounds().contains(Vec3d.ofCenter(this.lastPortalPos))) {
                final BlockState blockState = world.getBlockState(this.lastPortalPos);
                if (blockState.getBlock() == Blocks.NETHER_PORTAL) {
                    this.convertPortalFrame(
                        this.lastPortalPos.toImmutable(), blockState.get(NetherPortalBlock.AXIS), world
                    );
                }
            }
        }
    }

    @Override
    public void ghastly_wail$setLastPortalPos(BlockPos pos) {
        if (this.getBounds().contains(Vec3d.ofCenter(pos))) {
            this.lastPortalPos = pos.toImmutable();
        }
    }

    @Unique
    private void convertPortalFrame(BlockPos portalPos, Direction.Axis axis, ServerWorld world) {
        while((this.posMatchesBlock(portalPos, world, Blocks.NETHER_PORTAL))) {
            portalPos = portalPos.up();
        }

        BlockPos curFramePos = portalPos;
        final Direction backwards = Direction.from(axis, Direction.AxisDirection.NEGATIVE);
        final Direction forwards = backwards.getOpposite();
        final List <BlockPos> framePosList = new LinkedList<>();
        /*
         *    <-|
         * [][][][]
         * []    []
         * []    []
         * []    []
         * [][][][]
         */
        //noinspection CollectionAddAllCanBeReplacedWithConstructor; I prefer symmetry
        framePosList.addAll(this.traverseSection(curFramePos, backwards, Direction.DOWN, world).getLeft());
        /*
         *      |->
         * [][][][]
         * []    []
         * []    []
         * []    []
         * [][][][]
         */
        Pair<List<BlockPos>, BlockPos> framePosListAndLastPos =
            this.traverseSection(portalPos, forwards, Direction.DOWN, world);
        framePosList.addAll(framePosListAndLastPos.getLeft());
        curFramePos = framePosListAndLastPos.getRight();
        /*
         * [][][][]---
         * []    [] |
         * []    []\/
         * []    []
         * [][][][]
         */
        framePosListAndLastPos = this.traverseSection(curFramePos.down(), Direction.DOWN, backwards, world);
        framePosList.addAll(framePosListAndLastPos.getLeft());
        curFramePos = framePosListAndLastPos.getRight();
        /*
         * [][][][]
         * []    []
         * []    []
         * []    []
         * [][][][]
         *     <-|
         */
        framePosListAndLastPos = this.traverseSection(
            curFramePos.offset(Direction.from(axis, Direction.AxisDirection.NEGATIVE)),
            backwards, Direction.UP,
            world
        );
        framePosList.addAll(framePosListAndLastPos.getLeft());
        curFramePos = framePosListAndLastPos.getRight();
        /*
         *    [][][][]
         *    []    []
         *  /\[]    []
         *  | []    []
         * ---[][][][]
         */
        framePosList.addAll(this.traverseSection(curFramePos.up(), Direction.UP, forwards, world).getLeft());

        boolean triggeredCriterion = false;
        for (final BlockPos framePos : framePosList) {
            if (shouldConvertObsidian(world)) {
                if (shouldBreakConvertedObsidian(world)) {
                    if (world.breakBlock(framePos, false)) {
                        Block.dropStack(world, framePos, new ItemStack(Items.CRYING_OBSIDIAN));
                    }
                } else {
                    world.setBlockState(framePos, Blocks.CRYING_OBSIDIAN.getDefaultState());
                }

                if (!triggeredCriterion) {
                    triggeredCriterion = true;
                    final double x = this.getPos().getX();
                    final double y = this.getPos().getY();
                    final double z = this.getPos().getZ();

                    for (final ServerPlayerEntity serverPlayerEntity : world.getNonSpectatingEntities(
                        ServerPlayerEntity.class, new Box(x, y, z, x, y, z).expand(10.0, 5.0, 10.0)
                    )) {
                        GhastlyWail.getMadeObsidianCry().trigger(serverPlayerEntity);
                    }
                }
            }
        }
    }

    // returns frame block pos list and last pos checked (corner)
    @Unique
    private Pair<List<BlockPos>, BlockPos> traverseSection(
        BlockPos framePos, Direction traversalDir, Direction innerDir, ServerWorld world
    ) {
        final List <BlockPos> frameBlockPosList = new LinkedList<>();
        while(this.posMatchesBlock(framePos.offset(innerDir), world, Blocks.NETHER_PORTAL)) {
            if (this.posMatchesBlock(framePos, world, Blocks.OBSIDIAN)) {
                frameBlockPosList.add(framePos);
            }

            framePos = framePos.offset(traversalDir);
        }
        // break corner if obsidian
        if (this.posMatchesBlock(framePos, world, Blocks.OBSIDIAN)) {
            frameBlockPosList.add(framePos);
        }

        return new Pair<>(frameBlockPosList, framePos);
    }

    @Unique
    private boolean posMatchesBlock(BlockPos pos, ServerWorld world, Block block) {
        return World.isValid(pos) && world.getBlockState(pos).getBlock() == block;
    }
}
