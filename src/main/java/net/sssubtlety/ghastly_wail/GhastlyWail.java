package net.sssubtlety.ghastly_wail;

import org.jetbrains.annotations.Nullable;

import net.minecraft.advancement.criterion.Criteria;

import java.util.Optional;

public class GhastlyWail {
    public static final String NAMESPACE = "ghastly_wail";

    @Nullable
    private static MadeObsidianCryCriterion MADE_OBSIDIAN_CRY;

    public static MadeObsidianCryCriterion getMadeObsidianCry() {
        return tryGetMadeObsidianCry().orElseThrow(
            () -> new IllegalStateException("MADE_OBSIDIAN_CRY accessed before initialization")
        );
    }

    public static Optional<MadeObsidianCryCriterion> tryGetMadeObsidianCry() {
        return Optional.ofNullable(MADE_OBSIDIAN_CRY);
    }

    static void init() {
        if (MADE_OBSIDIAN_CRY != null) {
            throw new IllegalStateException("Trying to re-initialize " + NAMESPACE);
        }

        MADE_OBSIDIAN_CRY = Criteria.register(NAMESPACE + ":made_obsidian_cry", new MadeObsidianCryCriterion());
    }
}
