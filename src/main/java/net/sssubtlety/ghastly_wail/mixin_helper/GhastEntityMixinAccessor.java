package net.sssubtlety.ghastly_wail.mixin_helper;

import net.minecraft.util.math.BlockPos;

public interface GhastEntityMixinAccessor {
    void ghastly_wail$setLastPortalPos(BlockPos pos);
}
